#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/time.h>
#include "graph.h"

blc_channel input;
blc_array history_array;

char const *with_option, *style_option;

int sx, sy;
int sampling_period;
int interactive_mode=0;
int final_columns_nb, final_rows_nb;
int offset=0;
int bands=1;
int dims_nb, *lengths;
double min, max;
enum {RUN, PAUSE};
int status=RUN;

void refresh_period_cb(char const*argument, void*){
	sampling_period=strtol(argument, NULL, 10)*1000;
}

int main(int argc, char *argv[]){
	char const *refresh_string, *min_str, *max_str, *history_str, *channel_name, *sample_string, *verbatim;
	char const *xmin_str, *xmax_str, *label_max_str;
	float xmin, xmax, lmax;
	int status=0;

	blc_program_set_description("Display the content of the blc_channel depending on its type and format");
	blc_program_add_option(&min_str, 'm', "min", "FL32", "minimal value", NULL);
	blc_program_add_option(&max_str, 'M', "max", "FL32", "maximal value", NULL);
	blc_program_add_option(&sample_string, 'p', "period", "UI32", "sampling period in ms or history", "10");
	blc_program_add_option(&style_option, 's', "style", "string", "gnuplot option set option (style, boxwidth,  ...)", "fill solid");
	blc_program_add_option(&verbatim, 't', "text", "string", "text directly put on the gnuplot set commandline", NULL);
	blc_program_add_option(&with_option, 'w', "with", "string", "gnuplot option with (lines, boxes, dots,...)", "lines");
	blc_program_add_option(&xmin_str, 'x', "xmin", "FL32", "minimal abscissa value", NULL);
	blc_program_add_option(&xmax_str, 'X', "xmax", "FL32", "maximal abscissa value", NULL);
	blc_program_add_option(&label_max_str, 'L', "label-max", "FL32", "display the absisca scale in order to have the last value as label-max ", NULL);
	blc_program_add_option(&history_str, 'H', "history", "UI32", "size of the history", NULL);
	blc_program_add_option(&refresh_string, 'P', "period", "UI32", "graph refresh period in ms", "100");
	blc_program_add_parameter(&channel_name, "blc_channel", 1, "channel you want to graph", NULL);

	//   blc_program_add_option(&size_str, 's', "size", "UI32[xUI32]", "size of the matrix", NULL);
	blc_program_init(&argc, &argv, blc_quit);

/*
	if (size_str){
		dims_nb=blc_sscan_dims(&lengths, size_str);
		switch(dims_nb){
		case 1: final_columns_nb=lengths[0];
		final_rows_nb=1;
		break;
		case 2:final_columns_nb=lengths[0];
		final_rows_nb=lengths[1];
		break;
		default:EXIT_ON_ERROR("Size of dim %d is not yet managed in size '%s'.", dims_nb, size_str);
		}
		FREE(lengths);
	}*/



	input.open(channel_name, BLC_CHANNEL_READ);

	if (min_str) min=strtof(min_str, NULL);
	else switch (input.type){
	case 'UIN8':case 'UI16':case 'UI32':case 'IN16':case 'IN32':case 'IN64':case 'FL32':case'FL64':min=0.0;break;
	case 'INT8':min=INT8_MIN;break;
	default: EXIT_ON_CHANNEL_ERROR(&input, "No default min value");
	}

	if (max_str) max=strtof(max_str, NULL);
	else switch (input.type){
	case 'UIN8':max=UINT8_MAX;break;
	case 'INT8':max=INT8_MAX;break;
	case 'FL32': case 'FL64':max=1.0;break;
	default: EXIT_ON_CHANNEL_ERROR(&input, "No default max value");
	}

	if (xmin_str) SSCANF(1, xmin_str, "%f", &xmin);
	else xmin=0;

	if (xmax_str) SSCANF(1, xmax_str, "%f", &xmax);
	else xmax=input.total_length;

	if (label_max_str) {
		SSCANF(1, label_max_str, "%f", &lmax);
		if (xmax_str==NULL) xmax=lmax; //We still want to see all the graph
	}
	else lmax=input.total_length;

	sampling_period=strtol(refresh_string, NULL, 10)*1000;

	fprintf(stderr, "=== %s: ", blc_program_name);
	if (history_str) fprintf(stderr, "sample period (%.3fms), history(%s), ",  sampling_period/1000.f, history_str);
	fprintf(stderr, "min(%.3lf), max(%.2lf) === \n", min, max);

	blc_command_add("p", refresh_period_cb,  "ms", "sampling period", NULL);

	blc_loop_try_add_waiting_semaphore(input.sem_new_data);
	// blc_loop_try_add_posting_semaphore(input.sem_ack_data);

	if (history_str) create_history_graph(&input, input.name+1, strtol(history_str, NULL, 10), sampling_period, strtol(sample_string, NULL, 10)*1000, min, max, verbatim);
	else create_graph(&input, input.name+1, sampling_period, min, max, xmin, xmax, lmax, verbatim);

	return (status);
}







