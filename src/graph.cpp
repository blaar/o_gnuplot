#include "blc_program.h"
#include "graph.h"
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>

typedef struct history:blc_array{
    blc_array *array;
    
    void init_timer();
    void start_refresh_history_chars();
    void start_refresh_history_floats();
    int  compute_next_k();
    
    int sampling_period, iteration, length;
    size_t element_size;
    unsigned long initial_us;
}type_history;

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
int k;

void type_history::init_timer(){
    struct timeval initial_timer;
    
    gettimeofday(&initial_timer, NULL);
    length=dims[dims_nb-1].length;
    initial_us = initial_timer.tv_sec*1000000+initial_timer.tv_usec;
    k=length-1;
}

/**This is in order to take into acount the absolute time. If an iteration is longer than it should, the next one will be shorter.*/

int type_history::compute_next_k(){
    div_t samples_div;
    struct timeval timer;
    
    gettimeofday(&timer, NULL);
    
    //sample_div.quot is the theoritical iteration.
    samples_div=div(timer.tv_sec*1000000+timer.tv_usec-initial_us, sampling_period);
    
    //We try to stay in the midle of the iteration
    if ((int)(iteration-samples_div.quot) >= 0)  usleep((iteration+1-samples_div.quot)*sampling_period+sampling_period/2-samples_div.rem);
    
    iteration++;
    
    return div(samples_div.quot, length).rem;
}

void type_history::start_refresh_history_chars(){
    int i, next_k;
    
    while(1){
        next_k=compute_next_k();
        pthread_mutex_lock(&mutex);
        if(next_k==k+1){ //normal case
            FOR(i, array->total_length) chars[i*length+next_k]=array->chars[i];
        }
        else if ((next_k-k)>1){
            color_eprintf(BLC_BRIGHT_YELLOW, "Missing '%d' samples\n", next_k-k);
            FOR(i, array->total_length) memset(chars+i*length+k, array->chars[i], next_k-k+1);
        }
        else if (next_k==k){
            color_eprintf(BLC_BRIGHT_YELLOW, "Twice the same sample iteration %d\n", iteration);
        }
        else FOR(i, array->total_length){ //It accross the end of the buffer
            memset(chars+i*length+k, array->chars[i], length-k);
            memset(chars+i*length, array->chars[i], next_k+1);
        }
        k=next_k;
        pthread_mutex_unlock(&mutex);
    }
}

void type_history::start_refresh_history_floats(){
    int i, j, next_k;
    
    while(1){
        next_k=compute_next_k();
        pthread_mutex_lock(&mutex);
        if(next_k==k+1){ //normal case
            FOR(i, array->total_length) floats[i*length+next_k]=array->floats[i];
        }
        else if ((next_k-k)>1){
            color_eprintf(BLC_BRIGHT_YELLOW, "Missing '%d' samples\n", next_k-k);
            FOR(i, array->total_length) for(j=k; j!=next_k+1; j++) floats[i*length+j]=array->floats[i];
        }
        else if (next_k==k){
            color_eprintf(BLC_BRIGHT_YELLOW, "Twice the same sample iteration %d\n", iteration);
        }
        else FOR(i, array->total_length){ //It accross the end of the buffer
            for(j=k; j!=length; j++) floats[i*length+j]=array->floats[i];
            for(j=0; j!=next_k+1; j++) floats[i*length+j]=array->floats[i];
        }
        k=next_k;
        pthread_mutex_unlock(&mutex);
    }
}

static void *refresh_history_cb(void *history_pt){
    type_history *history=(type_history*)history_pt;
    
    history->init_timer();
    
    switch (history->type) {
        case 'UIN8': case 'INT8':
            history->start_refresh_history_chars();
            break;
        case 'FL32':
            history->start_refresh_history_floats();
            break;
        default:
            break;
    }
    return NULL;
}

static void init_term(FILE *pipef, char const *title, char const* verbatim){
    fprintf(pipef, "set term qt 1 noraise\n"); //Keep focus on the calling terminal
    fprintf(pipef, "set title '%s'\n", title); //axis x en y  only
    fprintf(pipef, "set border 3\n"); //axis x en y  only
    fprintf(pipef, "set title font ',20'\n");
    fprintf(pipef, "set label font ',10'\n");
    fprintf(pipef, "set key font ',10'\n");
    fprintf(pipef, "set terminal qt noenhanced\n");//avoid interpretation of '_'
    fprintf(pipef, "set style %s\n", style_option);
    fprintf(pipef, "set boxwidth 0.9 relative\n");
    fprintf(pipef, "set grid\n");
    if (verbatim) fprintf(pipef, "%s\n", verbatim);
}

void create_history_graph(blc_array *array, char const *title, int history_length, int refresh_period, int sampling_period, float min, float max, char const *verbatim){
    char const *gnuplot_format=NULL;
    char command[LINE_MAX];
    
    char code;
    int i, offset, columns_nb=0;
    FILE *pipef;
    pthread_t thread;
    
    type_history history;
    
    switch (array->dims_nb){
        case 0:
            columns_nb=1;
            break;
        case 1:
            columns_nb=array->dims[0].length;
            break;
        default:
            EXIT_ON_ARRAY_ERROR(array, "Too many dims");
            break;
    }
    
    SYSTEM_ERROR_CHECK(pipef=popen("gnuplot", "w"), NULL, "Calling gnuplot");
    
    switch (array->type){
        case 'UIN8':
            gnuplot_format="%uchar";
            history.element_size=sizeof(uchar);
            break;
        case 'INT8':
            gnuplot_format="%char";
            history.element_size=sizeof(char);
            break;
        case 'FL32':
            gnuplot_format="%float";
            history.element_size=sizeof(float);
            break;
        default: EXIT_ON_ARRAY_ERROR(array, "The type is not managed");
    }
    
    init_term(pipef, title, verbatim);
    fprintf(pipef, "set xlabel 'time(s)'\n");

    
    if (min!=max) fprintf(pipef, "set yrange [%f:%f]\n", min, max);
    
    offset=snprintf(command, LINE_MAX, "plot  '-'  binary format='%s' record=%d using ($0*%f):1  title '%d' with %s",   gnuplot_format, history_length, sampling_period/1000000.f, 0, with_option);
    
    for(i=1; i!=columns_nb; i++){
        if (i<10) code=48+i; //01234567890
        else if (i<36) code=97+i-10; //abc...xyz
        else if (i<62) code=65+i-36; //ABC...XYZ
        else if (i==62) code='[';
        else if (i==63) code=']';
        else EXIT_ON_ERROR("Too many columns. You have '%d' max is 64", columns_nb);
        offset+=snprintf(command+offset, LINE_MAX-offset, ", '-'  binary format='%s' record=%d  u ($0*%f):1 t '%c' w %s", gnuplot_format, history_length, sampling_period/1000000.f, code, with_option);
    }
    
    history.init(array->type, array->format, 2, columns_nb, history_length);
    history.sampling_period=sampling_period;
    history.array=array;
    
    pthread_create(&thread, NULL, refresh_history_cb, &history);
    
    BLC_COMMAND_LOOP(refresh_period){
        fprintf(pipef, "\n%s\n", command);
        FOR(i, columns_nb){
            pthread_mutex_lock(&mutex);
            SYSTEM_ERROR_CHECK(fwrite(&history.chars[(i*history_length+k+1)*history.element_size], history.element_size, history_length-k-1, pipef), -1, NULL);
            SYSTEM_ERROR_CHECK(fwrite(&history.chars[(i*history_length)*history.element_size], history.element_size, k+1, pipef), -1, NULL);
            pthread_mutex_unlock(&mutex);
        }
        SYSTEM_ERROR_CHECK(fflush(pipef), -1, NULL);
    }
    SYSTEM_ERROR_CHECK(fclose(pipef), -1, NULL);
}

void create_graph(blc_channel *input, char const *title, int refresh_period, float min, float max, float xmin, float xmax, float label_max, char const *verbatim){
    char const *gnuplot_format=NULL;
    char command[LINE_MAX];
    char code;
    int i, offset, columns_nb=0, rows_nb=0;
    FILE *pipef;
    size_t element_size;
    char *buffer;
    
    SYSTEM_ERROR_CHECK(pipef=popen("gnuplot", "w"), NULL, "Calling gnuplot");
    
    switch (input->dims_nb){
        case 0:
            columns_nb=1;
            rows_nb=1;
            break;
        case 1:
            columns_nb=input->dims[0].length;
            rows_nb=1;
            break;
        case 2:
            columns_nb=input->dims[0].length;
            rows_nb=input->dims[1].length;
            break;
        default:
            EXIT_ON_ARRAY_ERROR(input, "Too many dims");
            break;
    }
    
    switch (input->type){
        case 'UIN8':
            gnuplot_format="%uchar";
            element_size=sizeof(uchar);
            break;
        case 'INT8':
            gnuplot_format="%char";
            element_size=sizeof(char);
            break;
        case 'FL32':
            gnuplot_format="%float";
            element_size=sizeof(float);
            break;
        default: EXIT_ON_ARRAY_ERROR(input, "The type is not managed");
    }
    
    init_term(pipef, title, verbatim);

  //  if (min!=max) fprintf(pipef, "set yrange [%f:%f]\n", min, max);
   // if (xmin!=xmax) fprintf(pipef, "set xrange [%f:%f]\n", xmin, xmax);

    if (input->dims_nb==2){
    //	fprintf(pipef, "set xrange [%f:%f]\n", 0, 10);
    	/*fprintf(pipef, "set yrange [%f:%f]\n", 0, 10);*/
  //  	fprintf(pipef, "set zrange [%f:%f]\n", min, max);
    	fprintf(pipef, "set view 30,190\n");


    	offset=snprintf(command, LINE_MAX, "splot '-' binary format='%s' array=%dx%d  title 'values' with pm3d", gnuplot_format, columns_nb, rows_nb);
    }
    else{
    	offset=snprintf(command, LINE_MAX, "plot '-' binary format='%s' record=%d using ($0*%f):1 title '%d' with %s", gnuplot_format, columns_nb, label_max/(float)columns_nb, 0, with_option);
    
    	for(i=1; i!=rows_nb; i++){
    		if (i<10) code=48+i;
    		else code=97+i-10;
    		offset+=snprintf(command+offset, LINE_MAX-offset, ", '-'  binary format='%s' record=%d  title '%c' with %s", gnuplot_format, columns_nb,  code, with_option);
    	}
    }
    
    buffer=MANY_ALLOCATIONS(input->size, char);
        
    BLC_COMMAND_LOOP(refresh_period){
        memcpy(buffer, input->data, input->size);
        if (input->sem_ack_data) SYSTEM_ERROR_CHECK(sem_post(input->sem_ack_data), -1, NULL);
        fprintf(pipef, "\n%s\n", command);
        SYSTEM_ERROR_CHECK(fwrite(buffer, element_size, columns_nb*rows_nb, pipef), -1, NULL);
        SYSTEM_ERROR_CHECK(fflush(pipef), -1, NULL);
    }
    SYSTEM_ERROR_CHECK(fclose(pipef), -1, NULL);
    FREE(buffer);
}



